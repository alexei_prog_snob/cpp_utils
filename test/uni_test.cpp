#include "uni_test.h"
#include "aps/cpp_utils/optparse.h"


namespace aps = alexei_prog_snob;

UNIT(valid_OptParse_test_addoption_and_print_help)
    aps::OptParse opt{std::move("unit_test")};
    std::vector<std::string> argsHelp{
        "-h"
    };
    aps::OptParse::opt_parse_result res = opt.AddOpt(std::move("test opt"), std::move("-t"), std::move("--test"));
    ASSERT_THAT_WITH_MESSAGE(
        "add opt test",
        res == aps::OptParse::opt_parse_result::SUCCEED);

    res = opt.ParseArgs(std::move(argsHelp));
    ASSERT_THAT_WITH_MESSAGE(
        "Try to print -h",
        res = aps::OptParse::opt_parse_result::HELP_VALUE_RECEIVED);
    
    argsHelp = {"--help"};
    ASSERT_THAT_WITH_MESSAGE(
        "Try to print --help",
        opt.ParseArgs(std::move(argsHelp)) == aps::OptParse::opt_parse_result::HELP_VALUE_RECEIVED);
END_UNIT

TEST_SUITE(cpp utils)
    TEST(valid_OptParse_test_addoption_and_print_help)
END_SUITE
