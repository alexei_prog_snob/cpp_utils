BIT=$(getconf LONG_BIT)
DIRECTORY=/usr/include/aps
INC_DIR="$DIRECTORY"/cpp_utils
LIB_NAME=apscpputils

make clean
make

if [ ! -d "$DIRECTORY" ]; then
  mkdir $DIRECTORY
fi

if [ ! -d "$INC_DIR" ]; then
  mkdir $INC_DIR
fi

cp ./inc/* $INC_DIR/
cp ./dynamic_lib/"$BIT"bit/lib"$LIB_NAME"_"$BIT"bit.so /usr/lib/lib"$LIB_NAME".so

cd ./test/
rm -rf *.out
rm -rf *.o
g++ uni_test.cpp -l"$LIB_NAME"
./a.out
