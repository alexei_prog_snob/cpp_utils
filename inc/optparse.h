#ifndef __OPTPARSE_H__
#define __OPTPARSE_H__

#include <stddef.h> // size_t

#include <string>   // string
#include <vector>   // vector
#include <memory>   // shared_ptr
#include <map>      // map

namespace alexei_prog_snob {

class OptParse {
public:
    explicit OptParse(std::string&& _appName);
    ~OptParse() = default;
    OptParse(const OptParse&) = delete;
    OptParse& operator=(const OptParse&) = delete;

    enum opt_parse_result {
        SUCCEED,
        INVALID_PARAM,
        INVALID_ARGS,
        FLAG_NOT_FOUND,
        ACTION_NOT_FOUND,
        NO_VALUE_RECEIVED,
        HELP_VALUE_RECEIVED,
    };

    opt_parse_result ParseArgs(const char* _args[], size_t _argSize);
    opt_parse_result ParseArgs(std::vector<std::string>&& _args);

    opt_parse_result AddOpt(
        std::string&& _help,
        std::string&& _flag,
        std::string&& _action
    );
private:
    enum flag_action {
        FLAG,
        ACTION,
        INVALID
    };

    struct OptData {
        std::string m_value;
        bool m_expectValue;
    };

    std::map<std::string, std::shared_ptr<OptData> > m_optMap;


    // Private Methods 
    std::shared_ptr<OptData> p_GenerateOptData();
    flag_action p_ValidateFlagActionInput(const std::string& _str);
    bool p_IsAction(const std::string& _str);
    bool p_IsFlag(const std::string& _str);
};

} // end alexei_prog_snob

#endif // __OPTPARSE_H__
