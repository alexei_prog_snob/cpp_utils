ARCH := ${ARGS}
# clang-format -style="{BasedOnStyle: llvm, IndentWidth: 4, AlignTrailingComments: true, PointerAlignment: Left}"
ifeq ($(ARCH), )
ARCH := $(shell getconf LONG_BIT)
endif

C_FLAGS_32 := -m32
C_FLAGS_64 := -m64

CC 			:= g++
INC 		:= ./inc/
SRC 		:= ./src/
OBJ 		:= ./obj/$(ARCH)bit/
DLIB 		:= ./dynamic_lib/$(ARCH)bit/
INCLUDES 	:= -I$(INC)
CFLAGS 		:= $(C_FLAGS_$(ARCH)) -fPIC -pedantic -ansi -Werror -Wall $(INCLUDES) -pthread -std=c++11

OBJECTS 	:= $(OBJ)optparse.o 

.PHONY : all

all : $(OBJECTS) $(DLIB)libapscpputils_$(ARCH)bit.so

$(DLIB)libapscpputils_$(ARCH)bit.so :$(OBJECTS)
	$(CC) $(CFLAGS) -shared $^ -o $@

$(OBJ)%.o : $(SRC)%.cpp $(INC)%.h
	@echo "Compile $@";$(CC) $(CFLAGS) -c $< -o $@

$(OBJ)%.o : $(SRC)%.cpp
	@echo "Compile $@";$(CC) $(CFLAGS) -c $< -o $@
	
clean:
	rm -rf $(OBJ)*.o
	rm -rf $(DLIB)*.so

