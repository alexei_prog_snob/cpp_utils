#include "optparse.h"
#include <iostream> // std::cout
#include <algorithm> // std::find, std::for_each
using namespace alexei_prog_snob;

const std::string H_STR = "-h";
const std::string HELP_STR = "--help";

OptParse::OptParse(std::string&& _appName) {
    std::shared_ptr<OptParse::OptData> data = p_GenerateOptData();
    data->m_value += "Usage:  " + _appName + " [options] file... \n \
    Options: \n \
    \t -h, --help \t\t Display this message \n \
    ";
    m_optMap[H_STR] = data;
    m_optMap[HELP_STR] = data;
}

OptParse::opt_parse_result OptParse::ParseArgs(const char* _args[], size_t _argSize) {
    std::vector<std::string> v;
    for (size_t i = 0; i < _argSize; ++i) {
        v.push_back(_args[i]);
    }

    return ParseArgs(std::move(v));
}

OptParse::opt_parse_result OptParse::ParseArgs(std::vector<std::string>&& _args) {
    if (
        std::find(_args.begin(), _args.end(), H_STR) != std::end(_args)     ||
        std::find(_args.begin(), _args.end(), HELP_STR) != std::end(_args)
    ) {
        std::cout<< m_optMap[H_STR]->m_value << std::endl;
        return HELP_VALUE_RECEIVED;
    }
    
    std::shared_ptr<OptParse::OptData> currentData = nullptr;
    OptParse::opt_parse_result parseResult = SUCCEED;

    auto lmbd = [&](const std::string& _arg)->void {
        OptParse::flag_action flagAction = p_ValidateFlagActionInput(_arg);
        switch (flagAction) {
        case OptParse::flag_action::FLAG:
        case OptParse::flag_action::ACTION:
        {
            if (currentData != nullptr) {
                parseResult = OptParse::opt_parse_result::NO_VALUE_RECEIVED;
                return;
            }

            auto foundFlagAction = m_optMap.find(_arg);
            if (foundFlagAction == m_optMap.end()) {
                parseResult = OptParse::opt_parse_result::FLAG_NOT_FOUND;
                return;
            }

            currentData = foundFlagAction->second;
            break;
        }
        case OptParse::flag_action::INVALID:{}
        default:
        {
            if (currentData == nullptr) {
                parseResult = OptParse::opt_parse_result::INVALID_ARGS;
                return;
            }
            currentData->m_value = std::move(_arg);
            currentData = nullptr;
            break;
        }
            
        }
    }; // end lambd

    std::for_each(_args.begin(), _args.end(), lmbd);
    return parseResult;
}

OptParse::opt_parse_result OptParse::AddOpt(
    std::string&& _help,
    std::string&& _flag,
    std::string&& _action) {
    
    if (p_IsFlag(_flag) != true || p_IsAction(_action) != true) {
        return OptParse::opt_parse_result::INVALID_PARAM;
    }

    m_optMap[HELP_STR]->m_value += "\t " + _flag + ", " + _action + " \t\t " + _help + "\n";

    std::shared_ptr<OptParse::OptData> data = p_GenerateOptData();
    m_optMap[_flag] = data;
    m_optMap[_action] = data;
    return OptParse::opt_parse_result::SUCCEED;
}



OptParse::flag_action OptParse::p_ValidateFlagActionInput(const std::string& _str) {
    if (_str.size() < 2) {
        return flag_action::INVALID;
    }

    if ('-' != _str[0]) {
        return flag_action::INVALID;
    }

    if ('-' == _str[1]) {
        if (_str.size() < 3) {
            return flag_action::INVALID;
        }
        return flag_action::ACTION;
    }

    return flag_action::FLAG;
}

bool OptParse::p_IsAction(const std::string& _str) {
    if (_str.size() < 3) {
        return false;
    }

    return '-' == _str[0] && '-' == _str[1] ? true : false;
}

bool OptParse::p_IsFlag(const std::string& _str) {
    if (_str.size() < 2) {
        return false;
    }
    return '-' == _str[0] ? true : false;
}

std::shared_ptr<OptParse::OptData> OptParse::p_GenerateOptData() {
    std::shared_ptr<OptParse::OptData> data(new OptData);
    data->m_value = "";
    data->m_expectValue = true;
    return data;
}